set :rails_env,    :production
set :application,  'hromadske_api'

server 'app1.hromadske.io',
  user:   'stapi',
  roles:  %w(app web db),
  ssh_options: {
    forward_agent:  true,
    port:           2000
  }

set :deploy_to,        "/home/stapi/app/"
set :nginx_conf_path,  '/home/stapi/conf/'
