set :rails_env,    :staging
set :application,  'hromadske_api'

server 'not-set',
  user:   'deploy',
  roles:  %w(app web db),
  ssh_options: {
    forward_agent:  true,
    port:           2000
  }

set :deploy_to,        "/home/deploy/app/"
set :nginx_conf_path,  '/home/deploy/conf/'
