lock '3.4.0'

Airbrussh.configure do |config|
  config.command_output = true
end

set :repo_url,             'git@bitbucket.org:digitalcrafters/hromadske_api.git'
set :application,          'hromadske_api'
set :user,                 'stapi'
set :linked_dirs,          fetch(:linked_dirs,  []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/assets')
set :linked_files,         fetch(:linked_files, []).push('config/database.yml')
set :forward_agent,        true
set :pty,                  true

namespace :deploy do
  task :start do
    on roles(:app) do
      execute :sudo, :service, 'nginx',         'start'
      execute :sudo, :service, 'hromadske_api', 'start'
    end
  end

  task :stop do
    on roles(:app) do
      execute :sudo, :service, 'nginx',         'stop'
      execute :sudo, :service, 'hromadske_api', 'stop'
    end
  end

  task :restart do
    on roles(:app) do
      execute :sudo, :service, 'nginx',         'reload'
      execute :sudo, :service, 'hromadske_api', 'stop'
      execute :sudo, :service, 'hromadske_api', 'start'
    end
  end

  after :published, :notify do
    invoke "maintenance:enable"
    invoke "robots:symlink"
    invoke "puma:symlink"
    invoke "nginx:symlink"
    invoke "initscript:install"
    invoke "logrotate:install"
    invoke "deploy:restart"
    invoke "maintenance:disable"
  end
end
