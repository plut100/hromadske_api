#!/bin/sh

### BEGIN INIT INFO
# Provides:          muztochka
# Required-Start:    $local_fs $remote_fs $network $syslog
# Required-Stop:     $local_fs $remote_fs $network $syslog
# Default-Start:     3 4 5
# Default-Stop:      0 1 6
# Short-Description: muztochka
# Description:       muztochka
# chkconfig: - 85 14
### END INIT INFO


### Environment variables
RAILS_ENV="staging"
export RAILS_ENV="staging"

app_user="deploy"
app_root="/home/deploy/app/current"
pid_path="$app_root/tmp/pids"
socket_path="$app_root/tmp/sockets"
puma_pid_path="$pid_path/puma.pid"
sidekiq_pid_path="$pid_path/sidekiq.pid"
wrapper_path="/home/$app_user/.rvm/wrappers/muztochka"

# Switch to the app_user if it is not he/she who is running the script.
if [ "$USER" != "$app_user" ]; then
  eval su - "$app_user" -c $(echo \")$0 "$@"$(echo \"); exit;
fi

# Switch to the gitlab path, exit on failure.
if ! cd "$app_root" ; then
 echo "Failed to cd into $app_root, exiting!";  exit 1
fi


### Init Script functions

## Gets the pids from the files
check_pids(){
  if ! mkdir -p "$pid_path"; then
    echo "Could not create the path $pid_path needed to store the pids."
    exit 1
  fi
  if [ -f "$puma_pid_path" ]; then
    ppid=$(cat "$puma_pid_path")
  else
    ppid=0
  fi
  if [ -f "$sidekiq_pid_path" ]; then
    spid=$(cat "$sidekiq_pid_path")
  else
    spid=0
  fi
}

wait_for_pids(){
  i=0;
  while [ ! -f $puma_pid_path -o ! -f $sidekiq_pid_path ]; do
    sleep 0.1;
    i=$((i+1))
    if [ $((i%10)) = 0 ]; then
      echo -n "."
    elif [ $((i)) = 301 ]; then
      echo "Waited 30s for the processes to write their pids, something probably went wrong."
      exit 1;
    fi
  done
  echo
}

check_pids

## Checks whether the different parts of the service are already running or not.
check_status(){
  check_pids
  if [ $ppid -ne 0 ]; then
    kill -0 "$ppid" 2>/dev/null
    puma_status="$?"
  else
    puma_status="-1"
  fi
  if [ $spid -ne 0 ]; then
    kill -0 "$spid" 2>/dev/null
    sidekiq_status="$?"
  else
    sidekiq_status="-1"
  fi
  if [ $puma_status = 0 -a $sidekiq_status = 0 ]; then
    app_status=0
  else
    # code 3 means 'program is not running'
    app_status=3
  fi
}

## Check for stale pids and remove them if necessary.
check_stale_pids(){
  check_status
  # If there is a pid it is something else than 0, the service is running if
  # *_status is == 0.
  if [ "$ppid" != "0" -a "$puma_status" != "0" ]; then
    echo "Removing stale Puma web server pid. This is most likely caused by the web server crashing the last time it ran."
    if ! rm "$puma_pid_path"; then
      echo "Unable to remove stale pid, exiting."
      exit 1
    fi
  fi
  if [ "$spid" != "0" -a "$sidekiq_status" != "0" ]; then
    echo "Removing stale Sidekiq pid. This is most likely caused by the Sidekiq crashing the last time it ran."
    if ! rm "$sidekiq_pid_path"; then
      echo "Unable to remove stale pid, exiting."
      exit 1
    fi
  fi
}

## If no parts of the service is running, bail out.
exit_if_not_running(){
  check_stale_pids
  if [ "$puma_status" != "0" -a "$sidekiq_status" != "0" ]; then
    echo "App is not running."
    exit
  fi
}

start() {
  check_stale_pids

  if [ "$puma_status" != "0" -a "$sidekiq_status" != "0" ]; then
    echo -n "Starting both the Puma and Sidekiq"
  elif [ "$puma_status" != "0" ]; then
    echo -n "Starting Sidekiq"
  elif [ "$sidekiq_status" != "0" ]; then
    echo -n "Starting Puma"
  fi

  if [ "$puma_status" = "0" ]; then
    echo "Puma is already running with pid $ppid, not restarting."
  else
    echo -n "Starting Puma"
    rm -f "$socket_path"/puma.socket 2>/dev/null
    RAILS_ENV=$RAILS_ENV $wrapper_path/bundle exec pumactl -F config/puma.rb start
  fi

  if [ "$sidekiq_status" = "0" ]; then
    echo "Sidekiq job dispatcher is already running with pid $spid, not restarting"
  else
    RAILS_ENV=$RAILS_ENV $wrapper_path/bundle exec sidekiq -e $RAILS_ENV -C config/sidekiq.yml -d
  fi

  wait_for_pids
  print_status
}

stop() {
  exit_if_not_running

  if [ "$puma_status" = "0" -a "$sidekiq_status" = "0" ]; then
    echo -n "Shutting down both Puma and Sidekiq"
  elif [ "$puma_status" = "0" ]; then
    echo -n "Shutting down Sidekiq"
  elif [ "$sidekiq_status" = "0" ]; then
    echo -n "Shutting down Puma"
  fi

  if [ "$puma_status" = "0" ]; then
     echo -n "Shutting down Puma"
     RAILS_ENV=$RAILS_ENV $wrapper_path/bundle exec pumactl -F config/puma.rb stop
  fi
  if [ "$sidekiq_status" = "0" ]; then
     echo -n "Shutting down Sidekiq"
     RAILS_ENV=$RAILS_ENV $wrapper_path/bundle exec sidekiqctl stop $sidekiq_pid_path
  fi


  while [ "$puma_status" = "0" -o "$sidekiq_status" = "0" ]; do
    sleep 1
    check_status
    printf "."
    if [ "$puma_status" != "0" -a "$sidekiq_status" != "0" ]; then
      printf "\n"
      break
    fi
  done

  sleep 1
  rm "$puma_pid_path" 2>/dev/null

  print_status
}

print_status() {
  check_status
  if [ "$puma_status" != "0" -a "$sidekiq_status" != "0" ]; then
    echo "App is not running."
    return
  fi
  if [ "$puma_status" = "0" ]; then
      echo "Puma with pid $ppid is running."
  else
      printf "Puma is \033[31mnot running\033[0m.\n"
  fi
  if [ "$sidekiq_status" = "0" ]; then
      echo "Sidekiq with pid $spid is running."
  else
      printf "Sidekiq is \033[31mnot running\033[0m.\n"
  fi
  if [ "$puma_status" = "0" -a "$sidekiq_status" = "0" ]; then
    printf "App is \033[32mup and running\033[0m.\n"
  fi
}

restart(){
  exit_if_not_running
  if [ "$ppid" = "0" ];then
    echo "The App Puma Web server is not running thus it can't be restarted."
    exit 1
  fi
  printf "Phased restart of App Puma... "
  RAILS_ENV=$RAILS_ENV $wrapper_path/bundle exec pumactl -F config/puma.rb phased-restart
  echo "Done."

  printf "Stop/Start of Sidekiq... "
  RAILS_ENV=$RAILS_ENV $wrapper_path/bundle exec sidekiqctl stop $sidekiq_pid_path
  RAILS_ENV=$RAILS_ENV $wrapper_path/bundle exec sidekiq -e $RAILS_ENV -C config/sidekiq.yml -d
  echo "Done."

  wait_for_pids
  print_status
}

### Finally the input handling.

case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart)
        restart
        ;;
  status)
        print_status
        exit $app_status
        ;;
  *)
        echo "Usage: service <app> {start|stop|restart|status}"
        exit 1
        ;;
esac

exit

