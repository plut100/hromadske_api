Rails.application.routes.draw do
  devise_for :users

  namespace :api, defaults: { format: :json } do
    mount_devise_token_auth_for 'User', at: 'auth'
    resources :posts
    resources :widgets
    resources :culture_widgets
    resources :news_widgets
    resources :top_widgets

    resources :topics
    resources :genres
    resources :users
    resources :roles, only: [:index]
    resources :programs
    resources :pictures

    get 'programs_posts', to: 'programs#posts'
    post 'programs/update_all', to: 'programs#update_all'
    put 'posts/update_image/:id', to: 'posts#update_image'
    put 'programs/update_image/:id', to: 'programs#update_image'
    put 'users/update_image/:id', to: 'users#update_image'
    put 'top_widgets/update_image/:id', to: 'top_widgets#update_image'
  end

  root to: "home#index"
end
