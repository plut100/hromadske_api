source 'https://rubygems.org'

gem 'rails', '4.2.4'
gem 'rails-api'

gem 'mysql2', '~> 0.3.18'

gem 'sass-rails', '~> 5.0'

gem 'uglifier', '>= 1.3.0'

gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'jquery-ui-rails', '~> 4.2.1'
gem 'jbuilder', '~> 2.0'
gem 'autoprefixer-rails'
gem 'slim'
gem 'non-stupid-digest-assets'
gem 'russian'

gem 'sdoc', '~> 0.4.0', group: :doc

gem 'faker'

gem 'paperclip'

gem 'rack-cors', :require => 'rack/cors'

gem "devise"
gem 'devise_token_auth'
gem 'omniauth'

gem 'friendly_id'
gem 'babosa'

gem 'aws-sdk', '2.2.1'
gem 'aws-sdk-v1', '1.66.0'

gem 'acts-as-taggable-on', '3.5.0'

group :development do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'simplecov', :require => false
  # Test
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  # Turns off the Rails asset pipeline log
  gem 'quiet_assets'
  # Mailer emulation
  gem 'letter_opener'
  # Debugging
  gem 'binding_of_caller'
  gem 'better_errors'

  gem 'pry-rails'
  gem 'pry-byebug'
  gem 'pry-stack_explorer'
  gem 'pry-rescue'

  gem 'awesome_print'

  gem 'capistrano', '3.4.0'
  gem 'capistrano-rvm', '0.1.2'
  gem 'capistrano-rails', '1.1.6'
  gem 'capistrano-bundler', '1.1.4'
  gem 'capistrano-maintenance',   '1.0.0', :require => false
  gem 'airbrussh',                '1.0.0', :require => false
  gem 'capistrano-rails-console', '1.0.2', :require => false
end

group :production do
  gem 'puma', '3.1.0'
end
