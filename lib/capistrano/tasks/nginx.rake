namespace :nginx do
  task :symlink do
    on roles(:web) do
      within fetch(:nginx_conf_path) do
        execute :ln, "-fs",
          "#{release_path}/config/custom/nginx/#{fetch(:stage)}.conf",
          "#{fetch(:application)}_#{fetch(:stage)}.conf"
      end
    end
  end
end
