namespace :deploy do
  task :cold do
    before 'deploy:migrate', 'deploy:db:setup'
    after 'deploy:migrate', 'deploy:db:seed'

    Rake::Task['deploy'].invoke
  end
end
