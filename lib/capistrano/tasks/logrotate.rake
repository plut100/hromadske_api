namespace :logrotate do
  task :install do
    on roles(:web) do
      execute(:sudo, :cp, "#{release_path}/config/custom/logrotate/#{fetch(:stage)}", "/etc/logrotate.d/hromadske_api")
      execute(:sudo, :chown, "root:root", "/etc/logrotate.d/hromadske_api")
      execute(:sudo, :chmod, "0644", "/etc/logrotate.d/hromadske_api")
    end
  end
end
