namespace :robots do
  task :symlink do
    on roles(:web) do
      execute :ln, "-fs",
        "#{release_path}/config/custom/robots/#{fetch(:stage)}",
        "#{release_path}/public/robots.txt"
    end
  end
end
