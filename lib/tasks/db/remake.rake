namespace :db do
  desc 'remake db'
  task :remake => [:drop, :create, :migrate]
end
