class Program < ActiveRecord::Base
  has_many :posts, dependent: :nullify

  has_attached_file :image, styles: { thumb: '300x331#', original: '1280x'},
                            default_style: :original,
                            default_url: ""

  validates_attachment :image,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  include DeletableAttachment
end
