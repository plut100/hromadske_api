class Topic < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  validates :name, :presence => true

  has_many :bonds, as: :bondable, dependent: :restrict_with_exception
  has_many :posts, through: :bonds

  accepts_nested_attributes_for :bonds, allow_destroy: true

  enum kind: %w( general region )

  def normalize_friendly_id(text)
    text.to_slug.normalize(transliterations: :ukrainian).to_s
  end
end
