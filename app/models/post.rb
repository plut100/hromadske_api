class Post < ActiveRecord::Base
  acts_as_taggable_on :tags

  extend FriendlyId
  friendly_id :title, use: :slugged
  validates :title, :presence => true

  belongs_to :creator, :class_name => "User", :foreign_key => 'creator_id'

  has_many :bullets
  has_many :bonds
  has_many :topics, :through => :bonds, :source => :bondable, :source_type => "Topic"
  has_many :genres, :through => :bonds, :source => :bondable, :source_type => "Genre"
  has_many :posts_users
  has_and_belongs_to_many :users

  accepts_nested_attributes_for :topics
  accepts_nested_attributes_for :genres
  accepts_nested_attributes_for :posts_users, allow_destroy: true
  accepts_nested_attributes_for :bonds, allow_destroy: true
  accepts_nested_attributes_for :bullets, allow_destroy: true

  belongs_to :program

  scope :unpublished, -> { where('publish > ?', Time.now) }
  scope :published, -> { pending.where('publish <= ?', Time.now) }

  enum image_type: %w( typical large )
  enum tizer_type: %w( picture youtube )
  enum template: %w( news story videos movie )
  enum main_page: %w( auto_feed main_post )
  enum media_type: %w( newz keep_up )
  enum icon: %w( video photo infographics multimedia document live_stream live_blog )
  enum icon_type: %w( urgent important updated )
  enum tizer_style: %w( outside inside )
  enum status: %w( progress ready pending )

  def normalize_friendly_id(text)
    text.to_slug.normalize(transliterations: :ukrainian).to_s
  end


  has_attached_file :image, styles: { thumb: '300x331#', medium: '1280x', wide: '1920x'},
                            default_style: :original,
                            default_url: ""
  validates_attachment :image,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  has_attached_file :image_tizer, styles: { thumb: '300x331#', medium: '1280x', wide: '1920x'},
                                  default_style: :original,
                                  default_url: ""
  validates_attachment :image_tizer,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  has_attached_file :image_social, styles: { thumb: '300x331#', medium: '1280x', wide: '1920x'},
                                   default_style: :original,
                                   default_url: ""
  validates_attachment :image_social,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  include DeletableAttachment
end
