class Genre < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  validates :name, :presence => true

  has_many :bonds, as: :bondable, dependent: :restrict_with_exception
  has_many :posts, through: :bonds, :source => :bondable, :source_type => "Genre"

  accepts_nested_attributes_for :bonds, allow_destroy: true

  enum kind: %w( history media )

  def normalize_friendly_id(text)
    text.to_slug.normalize(transliterations: :ukrainian).to_s
  end
end
