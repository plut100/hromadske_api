class Picture < ActiveRecord::Base
  has_attached_file :file, styles: { original: '1920x'},
                            default_style: :original,
                            default_url: ""

  validates_attachment :file,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }
end
