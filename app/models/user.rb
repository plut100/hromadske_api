class User < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable,
         :omniauthable

  enum role: %w( admin senior_editor editor efir_editor foto_editor coordinator journalist blogger )

  has_and_belongs_to_many :posts

  has_attached_file :avatar, styles: { thumb: '600x600#'},
                             default_style: :original,
                             default_url: ""

  validates_attachment :avatar,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  extend FriendlyId
  friendly_id :name, use: :slugged
  validates :name, :presence => true

  def normalize_friendly_id(text)
    text.to_slug.normalize(transliterations: :ukrainian).to_s
  end

  include DeletableAttachment
end
