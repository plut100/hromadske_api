class Widget < ActiveRecord::Base
  acts_as_taggable_on :tags

  has_many :bonds, as: :bondable, dependent: :destroy
  has_many :posts, -> { order 'bonds.created_at DESC' }, through: :bonds

  accepts_nested_attributes_for :bonds, allow_destroy: true

  enum kind: %w( widescreen important main auto wide package banner blogs slider top)

  scope :landing, -> { where(landing: true) }
  scope :culture, -> { where(culture: true) }
  scope :news, -> { where(news: true) }
  scope :active, -> { where(active: true) }

  has_attached_file :image, styles: { index: '940x400#'},
                            default_style: :original,
                            default_url: ""

  validates_attachment :image,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }
end
