class Bond < ActiveRecord::Base
  belongs_to :bondable, polymorphic: true, touch: true
  belongs_to :post
end
