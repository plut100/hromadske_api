module Api
  class ProgramsController < ApiController
    respond_to :json

    def index
      @programs = Program.order(position: :asc).all

      respond_with @programs
    end

    def posts
      @posts = Post.where.not(program_id: nil)

      respond_with @posts
    end

    def update_all
      programs = params[:programs]

      # find deleted Programs
      ids_from_params = programs.map { |x| x[:id] }
      deleted_programs = Program.where.not(id: ids_from_params)

      # remove deleted Programs from db
      deleted_programs.destroy_all if deleted_programs.present?

      # create new Programs or update existed
      programs.each_with_index do |program, index|
        p = Program.find_or_create_by(id: program[:id])
        p.update_attributes(position: index,
                            name: program[:name],
                            active: program[:active])
      end

      @programs = Program.order(position: :asc).all

      respond_to do |format|
        format.json { render 'update_all' }
      end
    end

    def create
      @program = Program.new(create_params)

      if @program.save
        respond_to do |format|
          format.json { render 'show' }
        end
      else
        render nothing: true, status: 201
      end
    end

    def update
      @program = Program.find(params[:id])

      if @program.update_attributes(update_params)
        respond_to do |format|
          format.json { render 'show' }
        end
      else
      end
    end

    def show
      @program = Program.find(params[:id])

      respond_to do |format|
        format.json { render 'show' }
      end
    end

    def destroy
      @program = Program.find(params[:id])
      @program.destroy

      render nothing: true, status: 201
    end

    def update_image
      @program = Program.find(params[:id])
      if @program.update_attributes(update_image_params)
        respond_to do |format|
          format.json { head :ok }
        end
      else
      end
    end

    private

    def create_params
      params.require(:program).permit(:name, :position, :active, :image_outside_link, :image)
    end

    def update_params
      params.permit(:name, :position, :active, :image_outside_link, :image)
    end

    def update_image_params
      params.permit(:image)
    end
  end
end
