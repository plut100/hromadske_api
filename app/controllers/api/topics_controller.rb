module Api
  class TopicsController < ApiController
    respond_to :json

    def index
      @topics = Topic.all

      respond_with @topics
    end

    def create
      @topic = Topic.create!(topic_params)

      render nothing: true, status: 201
    end

    def update
      @topic = Topic.find(params[:id])

      if @topic.update_attributes(topic_params)
        respond_to do |format|
          format.json { render @topic }
        end
      else
        respond_to do |format|
          format.json { render :json => { :errors => @topic.errors } }
        end
      end
    end

    def destroy
      @topic = Topic.find(params[:id])
      @topic.posts.clear
      @topic.destroy

      render nothing: true, status: 201
      rescue ActiveRecord::DeleteRestrictionError
        render :json => { :error => 'Можно удалить только ту Тему, у которой нет Материала' }
    end

    private

    def topic_params
      params["kind"] = params[:type]
      params.permit(:name, :active, :kind)
    end
  end
end
