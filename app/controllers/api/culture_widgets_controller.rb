module Api
  class CultureWidgetsController < ApiController
    respond_to :json

    def index
      @widgets = Widget.culture.order(position: :asc).all

      respond_with @widgets
    end

    def create
      widgets = params[:widgets]

      # find deleted Widgets
      ids_from_params = widgets.map { |x| x[:id] }
      deleted_widgets = Widget.culture.where.not(id: ids_from_params)

      # remove deleted Widgets from db
      deleted_widgets.destroy_all if deleted_widgets.present?

      # create new Widgets and update if exist
      widgets.each_with_index do |widget, index|
        w = Widget.find_or_create_by(id: widget[:id])
        w.update_attributes(position: index,
                            kind: widget[:kind].to_sym,
                            active: widget[:active],
                            culture: true)
      end

      @widgets = Widget.culture.order(position: :asc)

      respond_to do |format|
        format.json { render 'create' }
      end
    end

    def update
      @widget = Widget.find(params[:id])
      @widget.update_attributes(update_params)

      if params[:posts].present?
        posts = params[:posts]
        posts.each do |post|
          @post = Post.find(post[:id])
          Bond.create!(post: @post, bondable: @widget) if @post.present?
        end
      end

      respond_to do |format|
        format.json { render 'show' }
      end
    end

    def show
      @widget = Widget.find(params[:id])

      @posts = @widget.posts.order(updated_at: :desc).limit(1)
      @blogs = @widget.posts.order(updated_at: :desc).limit(3)
      @package = @widget.posts.order(updated_at: :desc).limit(6)
    end

    def destroy
      @widget = Widget.find(params[:id])
      @widget.destroy

      render nothing: true, status: 201
    end

    private

    def update_params
      params.permit(:kind, :position, :active, :tag_list, :culture)
    end
  end
end

