module Api
  class TopWidgetsController < ApiController
    respond_to :json

    def index
      @widgets = Widget.top.order(position: :asc).all

      respond_with @widgets
    end


    def create
      @widget = Widget.new(create_params)
      @widget.save

      if params[:posts].present?
        @widget.posts.clear

        posts = params[:posts]
        posts.each do |post|
          @post = Post.find(post[:id])
          Bond.create!(post: @post, bondable: @widget) if @post.present?
        end
      end

      respond_to do |format|
        format.json { render 'show' }
      end
    end

    def update
      @widget = Widget.find(params[:id])
      @widget.update_attributes(update_params)

      if params[:posts].present?
        @widget.posts.clear

        posts = params[:posts]
        posts.each do |post|
          @post = Post.find(post[:id])
          Bond.create!(post: @post, bondable: @widget) if @post.present?
        end
      end

      respond_to do |format|
        format.json { render 'show' }
      end
    end

    def show
      @widget = Widget.find(params[:id])
    end

    def destroy
      @widget = Widget.find(params[:id])
      @widget.posts.clear

      if @widget.kind != :widescreen
        @widget.destroy

        render nothing: true, status: 201
      end
    end

    def update_image
      @widget = Widget.find(params[:id])
      if @widget.update_attributes(update_image_params)
        respond_to do |format|
          format.json { head :ok }
        end
      else
      end
    end

    private

    def create_params
      params.permit(:position, :active, :tag_list, :image, :image_outside_link, :title, :url)
            .merge(kind: :top)
    end

    def update_params
      params.permit(:kind, :position, :active, :tag_list, :image, :image_outside_link, :title, :url)
    end

    def update_image_params
      params.permit(:image)
    end
  end
end

