module Api
  class PicturesController < ApiController
    respond_to :json

    def index
      @pictures = Picture.all

      respond_with @pictures
    end

    def create
      @picture = Picture.new(picture_params)

      if @picture.save
        respond_to do |format|
          format.json { render @picture, :only => [:file] }
        end
      else
        respond_to do |format|
          format.json { render :json => { :errors => @picture.errors } }
        end
      end
    end

    def destroy
      @picture = Picture.find(params[:id])
      @picture.destroy

      render nothing: true, status: 201
    end

    private

    def picture_params
      params.permit(:file)
    end
  end
end
