module Api
  class RolesController < ApiController
    respond_to :json

    def index
      @roles = User.roles

      respond_with @roles
    end
  end
end
