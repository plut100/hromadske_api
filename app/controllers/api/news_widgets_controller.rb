module Api
  class NewsWidgetsController < ApiController
    respond_to :json

    def index
      @widgets = Widget.news.order(position: :asc).all

      respond_with @widgets
    end

    def create
      widgets = params[:widgets]

      # find deleted Widgets
      ids_from_params = widgets.map { |x| x[:id] }
      deleted_widgets = Widget.news.where.not(id: ids_from_params)

      # remove deleted Widgets from db
      deleted_widgets.destroy_all if deleted_widgets.present?

      # create new Widgets and update if exist
      widgets.each_with_index do |widget, index|
        w = Widget.find_or_create_by(id: widget[:id])
        w.update_attributes(position: index,
                            kind: :top,
                            active: widget[:active],
                            schedule: widget[:schedule],
                            news: true)
      end

      @widgets = Widget.news.order(position: :asc)

      respond_to do |format|
        format.json { render 'create' }
      end
    end

    def destroy
      @widget = Widget.find(params[:id])
      @widget.posts.clear
      @widget.destroy

      render nothing: true, status: 201
    end
  end
end

