module Api
  class PostsController < ApiController
    respond_to :json

    def index
      @posts = Post.all

      respond_with @posts
    end

    def show
      @post = Post.find(params[:id])
    end

    def create
      @post = Post.new(create_params)
      @post.save

      if params[:genres].present?
        if params[:genres] == "delete"
          Bond.where(post: @post, bondable_type: "Genre").destroy_all
        else
          # destroy old Genres
          genres = Bond.where(post: @post, bondable_type: "Genre")
          genres.destroy_all if genres.present?

          # create new Genres
          genres = params[:genres]
          genres.each do |genre|
            Bond.create!(post: @post, bondable_type: "Genre", bondable_id: genre[:id])
          end
        end
      end

      if params[:topics].present?
        if params[:topics] == "delete"
          Bond.where(post: @post, bondable_type: "Topic").destroy_all
        else
          # destroy old Topics
          topics = Bond.where(post: @post, bondable_type: "Topic")
          topics.destroy_all if topics.present?

          # creae new Topics
          topics = params[:topics]
          topics.each do |topic|
            Bond.create!(post: @post, bondable_type: "Topic", bondable_id: topic[:id])
          end
        end
      end

      respond_to do |format|
        format.json { render @post, :only => [:id] }
      end
    end

    def update
      @post = Post.find(params[:id])
      @post.update_attributes(update_params)
      @post.save

      if params[:genres].present?
        if params[:genres] == "delete"
          Bond.where(post: @post, bondable_type: "Topic").destroy_all
        else
          # destroy old Genres
          genres = Bond.where(post: @post, bondable_type: "Genre")
          genres.destroy_all if genres.present?

          # creae new Genres
          genres = params[:genres]
          genres.each do |genre|
            Bond.create!(post: @post, bondable_type: "Genre", bondable_id: genre[:id])
          end
        end
      end

      if params[:topics].present?
        if params[:topics] == "delete"
          Bond.where(post: @post, bondable_type: "Topic").destroy_all
        else
          # destroy old Topics
          topics = Bond.where(post: @post, bondable_type: "Topic")
          topics.destroy_all if topics.present?

          # creae new Topics
          topics = params[:topics]
          topics.each do |topic|
            Bond.create!(post: @post, bondable_type: "Topic", bondable_id: topic[:id])
          end
        end
      end

      if params[:users].present?
        # destroy all relations
        @post.users.clear

        users = params[:users]
        users.each do |user|
          author = @post.posts_users.build(user_id: user[:id])
          @post.posts_users << author
        end
      end

      respond_to do |format|
        format.json { render @post }
      end
    end

    def update_image
      @post = Post.find(params[:id])
      if @post.update_attributes(update_image_params)
        respond_to do |format|
          format.json { head :ok }
        end
      else
      end
    end

    def destroy
      @post = Post.find(params[:id])
      @post.destroy

      render nothing: true, status: 201
    end

    private

    def create_params
      if params[:publish].present?
        params["publish"] = Time.at(params[:publish] / 1000)
      end

      params.permit(:title, :status, :body, :image_type, :lead_status,
                    :lead_text, :media_type, :caption, :copyright, :link_to_picture,
                    :template, :publish, :badge, :important, :main_page,
                    :culture, :icon, :icon_type, :tizer_style, :tizer_type, :status,
                    :body, :creator_id, :program_id, :blog, :seo_title,
                    :seo_description, :tizer_title, :tizer_text,
                    :image,
                    :image_outside_link,
                    :image_tizer,
                    :image_tizer_outside_link,
                    :image_social,
                    :image_social_outside_link,
                    :tag_list,
                    :social_twitter_title,
                    :social_badge,
                    :bullets_attributes => [:id, :text, :_destroy, :post_id])
                    .merge(creator: current_user)
                    .merge(program: Program.find_by(name: params[:program]))
    end


    def update_params
      if params[:publish].present?
        params["publish"] = Time.at(params[:publish] / 1000)
      end

      params.permit(:publish,
                    :title,
                    :image_type,
                    :status,
                    :lead_status,
                    :lead_text,
                    :lead_status,
                    :media_type,
                    :caption,
                    :copyright,
                    :link_to_picture,
                    :badge,
                    :important,
                    :main_page,
                    :culture,
                    :icon,
                    :icon_type,
                    :tizer_style,
                    :tizer_title,
                    :tizer_type,
                    :tizer_text,
                    :blog,
                    :body,
                    :image,
                    :image_outside_link,
                    :image_tizer,
                    :image_tizer_outside_link,
                    :image_social,
                    :image_social_outside_link,
                    :tag_list,
                    :seo_title,
                    :seo_description,
                    :social_twitter_title,
                    :social_badge,
                    :bullets_attributes => [:id, :text, :_destroy, :post_id])
                    .merge(program: Program.find_by(name: params[:program]))
    end

    def update_image_params
      params.permit(:image, :image_tizer, :image_social)
    end
  end
end
