module Api
  class GenresController < ApiController
    respond_to :json

    def index
      @genres = Genre.all

      respond_with @genres
    end

    def create
      @genre = Genre.create!(genre_params)

      render nothing: true, status: 201
    end

    def update
      @genre = Genre.find(params[:id])

      if @genre.update_attributes(genre_params)
        respond_to do |format|
          format.json { render @genre }
        end
      else
        respond_to do |format|
          format.json { render :json => { :errors => @genre.errors } }
        end
      end
    end

    def destroy
      @genre = Genre.find(params[:id])
      @genre.posts.clear
      @genre.destroy

      render nothing: true, status: 201
      rescue ActiveRecord::DeleteRestrictionError
        render :json => { :error => 'Можно удалить только тот Жанр, у которой нет Материала' }
    end

    private

    def genre_params
      params["kind"] = params[:type]
      params.permit(:name, :active, :kind)
    end
  end
end
