module Api
  class UsersController < ApiController
    respond_to :json

    def show
      @user = User.find(params[:id])
    end

    def index
      if params[:authors]
        @users = User.where(role: [7, 8])
      else
        @users = User.all
      end

      respond_with @users
    end

    def create
      @user = User.new(user_params)
      @user.skip_confirmation!

      if @user.save
        respond_to do |format|
          format.json { render @user, :only => [:id] }
        end
      else
        respond_to do |format|
          format.json { render :json => { :errors => @user.errors } }
        end
      end
    end

    def update
      @user = User.find(params[:id])

      if @user.update_attributes(update_params)
        respond_to do |format|
          format.json { render @user }
        end
      else
        respond_to do |format|
          format.json { render :json => { :errors => @user.errors } }
        end
      end
    end

    def destroy
      @user = User.find(params[:id])
      @user.destroy

      render nothing: true, status: 201
    end


    def update_image
      @user = User.find(params[:id])
      if @user.update_attributes(update_image_params)
        respond_to do |format|
          format.json { head :ok }
        end
      else
      end
    end

    private

    def user_params
      params.permit(:name, :email, :role, :about, :avatar,
                    :twitter, :facebook, :personal_link_first,
                    :personal_link_second,
                    :password, :password_confirmation)
    end

    def update_params
      params.permit(:name, :email, :role, :about, :avatar,
                    :twitter, :facebook, :personal_link_first,
                    :personal_link_second)
    end

    def update_image_params
      params.permit(:avatar)
    end

    def update_password
      params.permit(:password, :password_confirmation)
    end
  end
end
