class ApplicationController < ActionController::Base

  skip_before_filter :verify_authenticity_token, if: ->(c) { c.request.format == 'application/json' }
  before_filter :allow_ajax_request_from_other_domains


  def allow_ajax_request_from_other_domains
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = '*'
  end
end
