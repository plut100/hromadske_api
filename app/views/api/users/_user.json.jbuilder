json.id   user.id
json.name user.name
json.role user.role
json.email user.email
json.avatar user.avatar.url
json.facebook user.facebook
json.twitter user.twitter
json.about user.about
