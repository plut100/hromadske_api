json.id    @user.id
json.role  @user.role
json.name  @user.name
json.email @user.email
json.avatar @user.avatar.url
json.facebook @user.facebook
json.twitter @user.twitter
json.about @user.about

