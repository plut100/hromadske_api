json.id @widget.id
json.kind @widget.kind
json.active @widget.active

if @widget.posts.present?
  if @widget.auto? or @widget.slider?
    json.content @widget.posts.order(updated_at: :desc) do |post|
      json.id post.id
      json.title post.tizer_title
      json.date_change post.updated_at.to_i * 1000
      json.author post.users.last.name if post.users.present?
    end
else
  if @widget.auto? or @widget.slider?
    json.set! :content, [{}]
  end
end
