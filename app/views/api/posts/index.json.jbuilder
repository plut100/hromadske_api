json.set! :progress do
  json.array! @posts.progress.each do |post|
    json.partial! post
  end
end

json.set! :ready do
  json.array! @posts.ready.each do |post|
    json.partial! post
  end
end

json.set! :pending do
  json.array! @posts.pending.unpublished.each do |post|
    json.partial! post
  end
end

json.set! :published do
  json.array! @posts.published.each do |post|
    json.partial! post
  end
end

