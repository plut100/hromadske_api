json.id                   @post.id
json.title                @post.title
json.creator              @post.creator.name if @post.creator.present?
json.posted               @post.created_at.to_i * 1000
json.modified             @post.updated_at.to_i * 1000
json.views                @post.views
json.shares               @post.shares
json.status               @post.status
json.lead_status          @post.lead_status
json.lead_text            @post.lead_text
json.image_type           @post.image_type
json.template             @post.template
json.main_page            @post.main_page
json.media_type           @post.media_type
json.icon                 @post.icon
json.icon_type            @post.icon_type
json.tizer_style          @post.tizer_style
json.tizer_title          @post.tizer_title
json.tizer_text           @post.tizer_text
json.tizer_type           @post.tizer_type
json.caption              @post.caption
json.copyright            @post.copyright
json.badge                @post.badge
json.publish              @post.publish.to_i * 1000
json.important            @post.important
json.culture              @post.culture
json.blog                 @post.blog
json.image                @post.image.url
json.image_tizer          @post.image_tizer.url
json.image_social         @post.image_social.url
json.tag_list             @post.tag_list
json.seo_title            @post.seo_title
json.seo_description      @post.seo_description
json.social_badge         @post.social_badge
json.social_twitter_title @post.social_twitter_title

json.image_outside_link        @post.image_outside_link
json.image_tizer_outside_link  @post.image_tizer_outside_link
json.image_social_outside_link @post.image_social_outside_link

json.delete_image false
json.delete_image_tizer false
json.delete_image_social false

if @post.program.present?
  json.program    @post.program.name
  json.program_id @post.program.id
end

if @post.bullets.present?
  json.bullets_attributes do
    json.array! @post.bullets do |bullet|
      json.id       bullet.id
      json.text     bullet.text
      json._destroy false
    end
  end
end

if @post.topics.present?
  json.topics do
    json.array! @post.topics.uniq do |topic|
      json.id   topic.id
      json.name topic.name
    end
  end
end

if @post.genres.present?
  json.genres do
    json.array! @post.genres.uniq do |genre|
      json.id   genre.id
      json.name genre.name
      json.kind genre.kind
    end
  end
end

if @post.users.present?
  json.users do
    json.array! @post.users.uniq do |user|
      json.id       user.id
      json.name     user.name
      json._destroy false
    end
  end
end

json.body @post.body
