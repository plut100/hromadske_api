json.cache! post do
  json.id post.id
  json.title post.tizer_title
  json.creator post.creator.name if post.creator.present?
  json.author post.users.first.name if post.users.present?
  json.posted post.created_at.to_i * 1000
  json.modified post.updated_at.to_i * 1000
  json.views post.views
  json.shares post.shares
  json.genres post.genres.uniq.map(&:name).to_s
  json.topics post.topics.uniq.map(&:name).to_s
end



