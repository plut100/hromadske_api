json.id       @widget.id
json.kind     @widget.kind
json.landing  @widget.landing
json.culture  @widget.culture
json.news     @widget.news
json.position @widget.position
json.active   @widget.active
json.tag_list @widget.tag_list

if @widget.posts.present?
  if @widget.auto? or @widget.blogs?
    json.content @blogs do |post|
      json.id post.id
      json.title post.tizer_title
      json.modified post.updated_at.to_i * 1000
      json.author post.users.last.name if post.users.present?
    end
  elsif @widget.package?
    json.content @package do |post|
      json.id post.id
      json.title post.tizer_title
      json.modified post.updated_at.to_i * 1000
    end
  else
    json.content @posts do |post|
      json.id post.id
      json.title post.tizer_title
      json.modified post.updated_at.to_i * 1000
      json.author post.users.last.name if post.users.present?
    end
  end
else
  if @widget.auto? or @widget.blogs?
    json.set! :content, [{}, {}, {}]
  elsif @widget.package?
    json.set! :content, [{}, {}, {}, {}, {}, {}]
  end
end
