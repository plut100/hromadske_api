json.id widget.id
json.kind widget.kind
json.active widget.active


if widget.posts.present?
  if widget.auto? or widget.blogs?
    json.content widget.posts.limit(3) do |post|
      json.id post.id
      json.title post.tizer_title
      json.modified post.updated_at.to_i * 1000
      json.author post.users.last.name if post.users.present?
    end
  elsif widget.package?
    json.content widget.posts.limit(6) do |post|
      json.id post.id
      json.title post.tizer_title
      json.modified post.updated_at.to_i * 1000
    end

  elsif widget.main?
    if @main_post.present?
      json.content [@main_post] do |obj|
        json.id obj.id
        json.title obj.tizer_title
        json.modified obj.updated_at.to_i * 1000
      end
    end
  elsif widget.top?
    json.title widget.title
    json.content widget.posts.limit(6) do |post|
      json.id post.id
      json.title post.tizer_title
      json.modified post.updated_at.to_i * 1000
      json.author post.users.last.name if post.users.present?
    end
  else
    json.content widget.posts.limit(1) do |post|
      json.id post.id
      json.title post.tizer_title
      json.modified post.updated_at.to_i * 1000
      json.author post.users.last.name if post.users.present?
    end
  end
else
  if widget.auto? or widget.blogs?
    json.set! :content, [{}, {}, {}]
  elsif widget.package?
    json.set! :content, [{}, {}, {}, {}, {}, {}]
  end
end
