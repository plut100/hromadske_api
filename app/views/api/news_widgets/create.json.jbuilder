json.array! @widgets do |widget|
  json.id       widget.id
  json.kind     widget.kind
  json.active   widget.active
  json.schedule widget.schedule

  if widget.posts.present?
    if widget.auto?
      json.content widget.posts.order(updated_at: :desc) do |post|
        json.id post.id
        json.title post.title
        json.modified post.updated_at.to_i * 1000
        json.author post.author.name if post.author.present?
      end
    elsif widget.slider?
      json.content widget.posts.order(updated_at: :desc).limit(3) do |post|
        json.id post.id
        json.title post.title
        json.modified post.updated_at.to_i * 1000
        json.author post.author.name if post.author.present?
      end
    end
  else
    if widget.auto? or widget.slider?
      json.set! :content, [{}]
    end
  end
end
