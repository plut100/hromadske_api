json.id                 @widget.id
json.url                @widget.url
json.kind               @widget.kind
json.landing            @widget.landing
json.culture            @widget.culture
json.news               @widget.news
json.position           @widget.position
json.active             @widget.active
json.tag_list           @widget.tag_list
json.title              @widget.title
json.image              @widget.image.url
json.image_outside_link @widget.image_outside_link

if @widget.posts.present?
  json.content @widget.posts.limit(6) do |post|
    json.id       post.id
    json.title    post.tizer_title
    json.modified post.updated_at.to_i * 1000
    json.author   post.users.last.name if post.users.present?
  end
end
