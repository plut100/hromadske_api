json.array! @widgets do |widget|
  json.id       widget.id
  json.title    widget.title
  json.kind     widget.kind
  json.active   widget.active
  json.landing  widget.landing
  json.culture  widget.culture
  json.news     widget.news
  json.posted   widget.created_at.to_i * 1000
  json.modified widget.updated_at.to_i * 1000

  if widget.posts.present?
    json.content widget.posts.order(updated_at: :desc).limit(6) do |post|
      json.id       post.id
      json.title    post.tizer_title
      json.modified post.updated_at.to_i * 1000
      json.author   post.users.last.name if post.users.present?

      if post.users.present?
        json.authors post.users do |user|
          json.id   user.id
          json.name user.name
        end
      end
    end
  else
    json.set! :content, [{}, {}, {}]
  end
end

