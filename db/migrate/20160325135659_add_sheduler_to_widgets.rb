class AddShedulerToWidgets < ActiveRecord::Migration
  def change
    add_column :widgets, :schedule, :boolean, default: false
  end
end
