class CreateBonds < ActiveRecord::Migration
  def change
    create_table :bonds do |t|
      t.references :post, index: true
      t.references :bondable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
