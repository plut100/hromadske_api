class AddBadgeTwitterToSocialPosts < ActiveRecord::Migration
  def change
    add_column :posts, :social_badge, :string
    add_column :posts, :social_twitter_title, :string
  end
end
