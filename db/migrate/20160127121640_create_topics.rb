class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :slug
      t.string :name
      t.boolean :active, default: false

      t.integer :kind

      t.timestamps
    end
  end
end
