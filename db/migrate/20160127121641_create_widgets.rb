class CreateWidgets < ActiveRecord::Migration
  def change
    create_table :widgets do |t|
      t.integer :kind
      t.integer :position

      t.boolean :landing, default: false
      t.boolean :culture, default: false
      t.boolean :news, default: false

      t.attachment :image
      t.text :image_outside_link

      t.string :title
      t.string :url

      t.boolean :active, default: false

      t.timestamps
    end
  end
end
