class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :slug
      t.attachment :image
      t.text :image_outside_link
      t.attachment :image_tizer
      t.text :image_tizer_outside_link
      t.attachment :image_social
      t.text :image_social_outside_link

      t.string :title
      t.integer :image_type
      t.boolean :lead_status
      t.string :lead_text
      t.integer :media_type
      t.string :caption
      t.string :copyright
      t.text :link_to_picture

      t.integer :template

      t.datetime :publish
      t.string :badge

      t.boolean :important
      t.integer :main_page

      t.boolean :culture

      t.integer :icon
      t.integer :icon_type
      t.integer :tizer_style
      t.integer :tizer_type

      t.string :tizer_title
      t.text :tizer_text

      t.integer :status, default: 0, null: false

      t.text :body
      t.integer :creator_id

      t.references :program

      t.boolean :blog

      t.integer :views, default: 0
      t.integer :shares, default: 0

      t.string :seo_title
      t.text :seo_description

      t.timestamps
    end
  end
end
