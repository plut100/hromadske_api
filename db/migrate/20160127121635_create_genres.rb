class CreateGenres < ActiveRecord::Migration
  def change
    create_table :genres do |t|
      t.string :slug
      t.string :name
      t.boolean :active, default: false

      t.integer :kind

      t.timestamps
    end
  end
end


