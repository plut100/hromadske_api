class CreateBullets < ActiveRecord::Migration
  def change
    create_table :bullets do |t|
      t.references :post, index: true
      t.text :text

      t.timestamps
    end
  end
end
