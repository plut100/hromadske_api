class CreatePrograms < ActiveRecord::Migration
  def change
    create_table :programs do |t|
      t.string :slug
      t.string :name
      t.integer :position
      t.boolean :active, default: false

      t.text :image_outside_link
      t.attachment :image

      t.timestamps
    end
  end
end
