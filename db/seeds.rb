include ActionDispatch::TestProcess

def open_file(path)
  File.open(Rails.root.join('test/assets', path))
end

avatars = ['ava/1.png', 'ava/2.png', 'ava/3.png', 'ava/4.png', 'ava/5.png', 'ava/6.png']

genres = []
investigations = []
users = []
body = "
<h1><span style='font-size:24px'>С чего все началось</span></h1>

<p>&laquo;Сообщение для Xbox One, Microsoft или черт знает еще кого: ваши чертовы серверы возмутительно не работают. Вы заставите меня перейти на PlayStation, если не исправите это недоразумение. Никто не может поиграть онлайн. Чем ты занят, Гейтс? Почини свое дерьмо, мужик&raquo;, &mdash; говорит на камеру Снуп Догг. В комментарии к видео он также повторил угрозу перейти на PlayStation.</p>

<p>&nbsp;</p>

<p>Через несколько часов музыкант вновь выложил это же видео, сопроводив его уже сообщением о переходе на игровую консоль от конкурентов: &laquo;Xbox 1 чертовски плох для игры онлайн. Да, я говорю это &mdash; привет, PS4. Призываю всех моих людей завязать с Xbox&raquo;.</p>

<p>&nbsp;</p>

<p>За час до этого решения служба поддержки Xbox отчиталась о том, что неполадки устранены.</p>

<p>&nbsp;</p>

<p><img alt='' src='./images/Layer76.png' style='height:411px; max-width:100%; width:620px' /></p>

<p><span style='font-size:12px'>Фото любезно предоставлено Twentieth Century Fox <span style='color:#b2b2b2'>&copy; Twentieth Century Fox.</span></span></p>

<p>&nbsp;</p>

<div class='leftColl ng-scope'>
<p><img alt='' src='./images/Snoop.png' style='width:300px' /></p>

<p><span style='font-size:12px'>Кадр из &laquo;Один дома&raquo; (1990 год). Фото любезно предоставлено 20th Century Fox/John Hughes Entertainment</span></p>

<p><span style='color:#b2b2b2'><span style='font-size:12px'>&copy; Twentieth Century Fox</span></span></p>

<p>&nbsp;</p>
</div>

<p>&nbsp;&nbsp;&laquo;Сообщение для <a href='http://link'>Xbox One</a>, <a href='http://link'>Microsoft </a>или черт знает еще кого: ваши чертовы серверы возмутительно не работают. Вы заставите меня перейти на PlayStation, если не исправите это недоразумение. Никто не может поиграть онлайн. Чем ты занят, Гейтс? Почини свое дерьмо, мужик&raquo;, &mdash; говорит на камеру Снуп Догг. В комментарии к видео он также повторил угрозу перейти на <strong>PlayStation</strong>.</p>

<p>&nbsp;</p>

<p>Через несколько часов музыкант вновь выложил это же видео, сопроводив его уже сообщением о переходе на игровую консоль от конкурентов: <em>&laquo;Xbox 1 чертовски плох для игры онлайн. Да, я говорю это &mdash; привет, PS4. Призываю всех моих людей завязать с Xbox&raquo;.</em></p>

<p>За час до этого решения служба поддержки Xbox отчиталась о том, что неполадки устранены.</p>

<p>&nbsp;</p>

<p><span style='font-size:28px'><span style='color:#FF0000'>&laquo;</span> Ваши чертовы серверы возмутительно не работают. Вы заставите меня перейти на PlayStation, если не исправите это недоразумение. Никто не может поиграть онлайн. Чем ты занят, Гейтс? Почини свое дерьмо, мужик <span style='color:#FF0000'>&raquo;</span></span></p>

<hr />
<p>&nbsp;</p>

<p>&nbsp;</p>

<div class='leftColl ng-scope'>
<p><img alt='' src='./images/Layer62.png' style='height:170px; width:300px' /></p>

<p><strong>Крим відзвітував про &nbsp;третину завантаженості курортів</strong></p>

<p><span style='font-size:12px'><a href='http://link'><span style='color:#D3D3D3'>ЧИТАТИ БIЛЬШЕ</span></a></span></p>

<hr />
<p><a href='link'><img alt='' src='./images/back.png' style='height:27px; margin-left:20px; margin-right:20px; width:34px' /></a></p>
</div>

<p>&laquo;Сообщение для <a href='http://link'>Xbox One</a>, <a href='http://link'>Microsoft </a>или черт знает еще кого: ваши чертовы серверы возмутительно не работают. Вы заставите меня перейти на PlayStation, если не исправите это недоразумение. Никто не может поиграть онлайн. Чем ты занят, Гейтс? Почини свое дерьмо, мужик&raquo;, &mdash; говорит на камеру Снуп Догг. В комментарии к видео он также повторил угрозу перейти на <strong>PlayStation</strong>.</p>

<p>&nbsp;</p>

<p>Через несколько часов музыкант вновь выложил это же видео, сопроводив его уже сообщением о переходе на игровую консоль от конкурентов: <em>&laquo;Xbox 1 чертовски плох для игры онлайн. Да, я говорю это &mdash; привет, PS4. Призываю всех моих людей завязать с Xbox&raquo;.</em></p>

<p>За час до этого решения служба поддержки Xbox отчиталась о том, что неполадки устранены.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<div class='whide ng-scope'>
<p>&nbsp;</p>

<div class='autor'>
<p><img alt='' src='./images/Layer29.png' style='height:60px; width:60px' />&nbsp;&nbsp;<strong><span style='font-size:14px'>СЕРГIЙ ПОРОШЕНКО</span></strong>, <span style='font-size:14px'>главный исполнительный&nbsp;</span></p>

<p>&nbsp;</p>
</div>

<p><span style='font-size:28px'>&nbsp;<span style='color:#FF0000'>&laquo;</span> ваши чертовы серверы возмутительно не работают. Вы заставите меня перейти на PlayStation, если не исправите это недоразумение. Никто не может поиграть онлайн. Чем ты занят, Гейтс? Почини свое дерьмо, мужик <span style='color:#FF0000'>&raquo;</span></span></p>

<p>&nbsp;</p>
</div>

<p>&nbsp;</p>

<p>Что же нам так нравится в гаде-предпринимателе, этом образчике эгоцентризма и чрезмерной мужественной отваги? Возможно, нас привлекает мысль о том, что для того, чтобы быть действительно успешными, нам пришлось бы отказаться от доброты и повременить с великодушием, избавиться от всех своих человеческих забот и оборвать близкие отношения, дабы полностью сосредоточиться на цели шального успеха. Возможно, нам по душе мысль о том, что люди, которые &laquo;пробиваются&raquo; в бизнесе &ndash; это те, кто посылает остальной мир лесом, склонные к мазохизму лица 20 с лишним лет, спящие в офисе, бессердечно выманивающие деньги у лучших друзей и (подобно Стиву Джобса Дэнни Бойла или Марку Цукерберга Дэвида Финчера) верующие исключительно в собственный статус мессий, которые изменят мир.</p>

<div class='imgleft ng-scope'>
<p><img alt='' src='./images/Layer77.png' style='float:left; height:419px; margin-right:20px; width:620px' /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><span style='font-size:12px'>Кадр из &laquo;Один дома&raquo; (1990 год). Фото любезно предоставлено 20th Century Fox/John Hughes Entertainment</span></p>

<p><span style='color:#D3D3D3'><span style='font-size:12px'>&copy; Twentieth Century Fox</span></span></p>

<p>&nbsp;</p>
</div>

<p>Что же нам так нравится в гаде-предпринимателе, этом образчике эгоцентризма и чрезмерной мужественной отваги? Возможно, нас привлекает мысль о том, что для того, чтобы быть действительно успешными, нам пришлось бы отказаться от доброты и повременить с великодушием, избавиться от всех своих человеческих забот и оборвать близкие отношения, дабы полностью сосредоточиться на цели шального успеха. Возможно, нам по душе мысль о том, что люди, которые &laquo;пробиваются&raquo; в бизнесе &ndash; это те, кто посылает остальной мир лесом, склонные к мазохизму лица 20 с лишним лет, спящие в офисе, бессердечно выманивающие деньги у лучших друзей и (подобно Стиву Джобса Дэнни Бойла или Марку Цукерберга Дэвида Финчера) верующие исключительно в собственный статус мессий, которые изменят мир.</p>

<p>&nbsp;</p>

<div class='leftColl ng-scope'>
<p><img alt='' src='http://bzh.life/ckeditor_assets/pictures/4317/content_5.jpg' style='height:200px; width:300px' /><span style='font-size:12px'>Кадр из &laquo;Один дома&raquo; (1990 год). Фото любезно предоставлено 20th Century Fox/John Hughes Entertainment</span></p>

<p>&nbsp;</p>
</div>

<p>sadf sdaf sdfa sdf dsafsd f asdf sdfa sdf dfgdsf;g df;sgdf;slgk ;dfsgldfm slgdfgklsdfg dfgldsfhgoierhg kldfshogih dflksgn kdfhgoidfsg df</p>

<div class='autor ng-scope'>
<p><img alt='' src='http://bzh.life/ckeditor_assets/pictures/4317/content_5.jpg' style='height:33px; width:50px' />&nbsp; &nbsp; &nbsp; sdfsdfgdfg dfsa sdf sdf dfs</p>

<hr />
<p>&nbsp;</p>
</div>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><span style='font-size:24px'><strong>10 причины падения серверов XBOX:</strong></span></p>

<div class='greyList ng-scope' style='margin-left: 320px;'>
<ol>
  <li>​ Я Господь, Бог твой; да не будет у тебя других богов пред лицом Моим.</li>
  <li>Не делай себе кумира и никакого изображения того, что на небе вверху, что на земле внизу, и что в воде ниже земли. Не поклоняйся им и не служи им; ибо Я Господь, Бог твой, Бог ревнитель, наказывающий детей за вину отцов до третьего и четвёртого рода, ненавидящих Меня, и творящий милость до тысячи родов любящим Меня и соблюдающим заповеди Мои.</li>
  <li>Не произноси имени Господа, Бога твоего, напрасно; ибо Господь не оставит без наказания того, кто произносит имя Его напрасно.</li>
  <li>Помни день субботний, чтобы святить его. Шесть дней работай, и делай всякие дела твои; а день седьмой &mdash; суббота Господу, Богу твоему: не делай в оный никакого дела ни ты, ни сын твой, ни дочь твоя, ни раб твой, ни рабыня твоя, ни скот твой, ни пришелец, который в жилищах твоих. Ибо в шесть дней создал Господь небо и землю, море и все, что в них; а в день седьмый почил. Посему благословил Господь день субботний и освятил его.</li>
  <li>Почитай отца твоего и мать твою, чтобы продлились дни твои на земле, которую Господь, Бог твой, дает тебе.</li>
  <li>Не убивай.</li>
  <li>Не прелюбодействуй.</li>
  <li>Не кради.</li>
  <li>Не произноси ложного свидетельства на ближнего твоего.</li>
  <li>Не желай дома ближнего твоего; не желай жены ближнего твоего, ни раба его, ни рабыни его, ни вола его, ни осла его, ничего, что у ближнего твоего.</li>
</ol>

<p><span style='font-size:24px'><strong>Список фильмов:</strong></span></p>
</div>

<p><span style='color:#FF0000'>2008 &nbsp; &nbsp;&nbsp;</span>Король Сингх | Singh Is Kinng (Австралия)</p>

<p><span style='color:#FF0000'>2007 &nbsp; &nbsp; </span>Голые братья | The Naked Brothers Band (США)</p>

<p><span style='color:#FF0000'>2006</span> &nbsp; &nbsp; Квартал ужаса | Hood of Horror (США)</p>

<p><span style='color:#FF0000'>2011</span> &nbsp; &nbsp; Косяки | Weeds | Косячки (США)</p>

<p><span style='color:#FF0000'>2005</span> &nbsp; &nbsp; Правило №1: Шеф всегда прав | Boss&#39;n Up (США)</p>

<p><span style='color:#FF0000'>2005 &nbsp; &nbsp; </span>Обитатели | Tenants, The (Германия, США)</p>

<p><span style='color:#FF0000'>2005</span> &nbsp; &nbsp; Бундокс | The Boondocks (США, анимационный)</p>

<div class='bigNumber ng-scope'>
<p>&nbsp;</p>

<p><span style='font-size:24px'><strong>Список фильмов:</strong></span></p>
</div>

<ol>
  <li>Король Сингх | Singh Is Kinng (Австралия)</li>
  <li>Голые братья | The Naked Brothers Band (США)</li>
  <li>Квартал ужаса | Hood of Horror (США)
  <ol>
    <li>Косяки | Weeds | Косячки (США)</li>
    <li>Правило №1: Шеф всегда прав | Boss&#39;n Up (США)</li>
  </ol>
  </li>
  <li>Обитатели | Tenants, The (Германия, США)</li>
  <li>Бундокс | The Boondocks (США, анимационный)
  <ul>
    <li>Бундокс | The Boondocks (США, анимационный)</li>
    <li>Косяки | Weeds | Косячки (США)</li>
  </ul>
  </li>
  <li>Правило №1: Шеф всегда прав | Boss&#39;n Up (США)</li>
  <li>Король Сингх | Singh Is Kinng (Австралия)</li>
</ol>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
"


print 'Users.......'
User.create!(name: 'Адміністратор',
             email: 'admin@example.com',
             password: 'password',
             password_confirmation: 'password',
             confirmed_at: Time.zone.now,
             role: 0,
             avatar: open_file('ava/3.png'),
             about: "Hello world!",
             facebook: "http://fb.com",
             twitter: "http://twitter.com")

User.create!(name: 'Баштовий Андрій',
             email: 'andrii@example.com',
             password: 'password',
             password_confirmation: 'password',
             confirmed_at: Time.zone.now,
             role: 1,
             avatar: open_file('ava/1.png'),
             about: "Hello world!",
             facebook: "http://fb.com",
             twitter: "http://twitter.com")

User.create!(name: 'Кутєпов Богдан',
             email: 'bogdan@example.com',
             password: 'password',
             password_confirmation: 'password',
             confirmed_at: Time.zone.now,
             role: 1,
             avatar: open_file('ava/5.png'),
             about: "Hello world!",
             facebook: "http://fb.com",
             twitter: "http://twitter.com")

30.times do
  User.create!(name: Faker::Name.name,
               email: Faker::Internet.safe_email,
               password: 'password',
               password_confirmation: 'password',
               confirmed_at: Time.zone.now,
               role: rand(1..7),
               avatar: open_file(avatars.sample),
               about: Faker::Lorem.paragraph(2),
               facebook: "http://fb.com",
               twitter: "http://twitter.com")
end

puts "..... OK \n\n"



print 'Topics.......'
Topic.create!(name: 'Політика',    active: true, kind: :general)
Topic.create!(name: 'Суспільство', active: true, kind: :general)
Topic.create!(name: 'Економіка',   active: true, kind: :general)
Topic.create!(name: 'Культура',    active: true, kind: :general)
Topic.create!(name: 'Війна',       active: true, kind: :general)
Topic.create!(name: 'Тест',        kind: :general)
Topic.create!(name: 'Україна',     active: true, kind: :region)
Topic.create!(name: 'Світ',        active: true, kind: :region)
puts "..... OK \n\n"

print 'Genres.......'
Genre.create!(name: 'Репортаж', kind: 0, active: true)
Genre.create!(name: "Інтерв’ю", kind: 0, active: true)
Genre.create!(name: 'Розслідування', kind: 0, active: true)
Genre.create!(name: 'Аналітика', kind: 0, active: true)
Genre.create!(name: 'Огляд', kind: 0, active: true)

Genre.create!(name: 'Фоторепортаж', kind: 1, active: true)
Genre.create!(name: 'Відео', kind: 1, active: true)
Genre.create!(name: 'Інфографіка', kind: 1, active: true)
Genre.create!(name: 'Фільм', kind: 1, active: true)
Genre.create!(name: 'Тест', kind: 1)
puts "..... OK \n\n"

print 'Programs.......'
Program.create!(name: 'Програма 1', active: true, position: 0, image: open_file('pr/prog1.png'))
Program.create!(name: 'Програма 2', active: false, position: 1, image: open_file('pr/prog2.png'))
Program.create!(name: 'Програма 3', active: true, position: 2, image: open_file('pr/prog3.png'))
Program.create!(name: 'Програма 4', active: true, position: 3, image: open_file('pr/prog4.png'))
Program.create!(name: 'Програма 5', active: true, position: 4, image: open_file('pr/prog5.png'))
Program.create!(name: 'Програма 6', active: false, position: 5, image: open_file('pr/prog6.png'))
Program.create!(name: 'Програма 7', active: true, position: 6, image: open_file('pr/prog7.png'))
Program.create!(name: 'Програма 8', active: false, position: 7, image: open_file('pr/prog8.png'))
Program.create!(name: 'Програма 9', active: true, position: 8, image: open_file('pr/prog9.png'))
Program.create!(name: 'Програма 10', active: true, position: 9, image: open_file('pr/prog4.png'))
puts "..... OK \n\n"

image_link = "http://hochu.ua/pictures_ckfinder/images/1359634407_vitaliy-klichko.jpeg"
image_link_2 = "http://wallpapersonthe.net/wallpapers/b/2560x1440/2560x1440-politics_sheep_animal-11849.jpg"

print 'Widgets.......'
widget = Widget.create!(kind: :widescreen,
                        position: 0,
                        active: true,
                        landing: true)
post = Post.create!(
  image: open_file('img/1.jpg'),
  image_tizer: open_file('img/1.jpg'),
  image_social: open_file('img/1.jpg'),
  image_outside_link: image_link,
  image_tizer_outside_link: image_link_2,
  image_social_outside_link: image_link_2,
  tizer_type: :picture,
  program_id: rand(1..4),
  creator_id: rand(1..3),
  title: Faker::Lorem.paragraph(1),
  tizer_title: Faker::Lorem.paragraph(1),
  tizer_text: Faker::Lorem.paragraph(1),
  status: :pending,
  created_at: Time.now-1.month,
  updated_at: Time.now-1.week,
  body: body,
  lead_status: true,
  lead_text: Faker::Lorem.paragraph(1),
  image_type: rand(0..1),
  template: rand(0..3),
  main_page: rand(0..1),
  media_type: rand(0..1),
  icon: rand(0..6),
  icon_type: rand(0..2),
  tizer_style: rand(0..1),
  caption: Faker::Lorem.paragraph(1),
  copyright: "Hello world!",
  publish: Time.now-1.month,
  badge: "Cool awesome Badge!",
  important: true,
  culture: true,
  blog: true,
  users: [ User.first, User.last ],
  tag_list: "скандалы, интриги, расследования",
)
Bond.create!(post: post, bondable: widget)
Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

2.times do
  Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
end

5.times do
  Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
end

widget = Widget.create!(kind: :important,
                        position: 1,
                        active: true,
                        landing: true,
                        tag_list: "скандалы, тусы, гороскопы")
post = Post.create!(
  image: open_file('img/2.jpg'),
  image_tizer: open_file('img/2.jpg'),
  image_social: open_file('img/2.jpg'),
  image_outside_link: image_link,
  image_tizer_outside_link: image_link,
  image_social_outside_link: image_link,
  creator_id: rand(1..3),
  tizer_type: :picture,
  title: Faker::Lorem.paragraph(1),
  tizer_title: Faker::Lorem.paragraph(1),
  tizer_text: Faker::Lorem.paragraph(1),
  status: :pending,
  created_at: Time.now-1.month,
  updated_at: Time.now-1.week,
  body: body,
  lead_status: false,
  lead_text: Faker::Lorem.paragraph(1),
  image_type: rand(0..1),
  template: rand(0..3),
  main_page: rand(0..1),
  media_type: rand(0..1),
  icon: rand(0..6),
  icon_type: rand(0..2),
  tizer_style: rand(0..1),
  caption: Faker::Lorem.paragraph(1),
  copyright: "Hello world!",
  publish: Time.now-1.month,
  badge: "Cool awesome Badge!",
  important: false,
  culture: true,
  blog: false,
  users: [ User.second, User.third ],
  tag_list: "вчерашний, завтрашний, сегодня"
)
Bond.create!(post: post, bondable: widget)
Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])
3.times do
  Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
end

6.times do
  Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
end


widget = Widget.create!(kind: :main,
                        position: 2,
                        active: true,
                        landing: true,
                        tag_list: "вчерашний, завтрашний, сегодня, скандалы")
post = Post.create!(
  image: open_file('img/3.jpg'),
  image_tizer: open_file('img/3.jpg'),
  image_social: open_file('img/3.jpg'),
  image_outside_link: image_link,
  image_tizer_outside_link: image_link,
  image_social_outside_link: image_link,
  creator_id: rand(1..3),
  title: Faker::Lorem.paragraph(1),
  tizer_title: Faker::Lorem.paragraph(1),
  tizer_text: Faker::Lorem.paragraph(1),
  status: :pending,
  tizer_type: :picture,
  created_at: Time.now-1.month,
  updated_at: Time.now-1.week,
  body: body,
  lead_status: true,
  lead_text: Faker::Lorem.paragraph(1),
  image_type: rand(0..1),
  template: rand(0..3),
  main_page: rand(0..1),
  media_type: rand(0..1),
  icon: rand(0..6),
  icon_type: rand(0..2),
  tizer_style: rand(0..1),
  caption: Faker::Lorem.paragraph(1),
  copyright: "Hello world!",
  publish: Time.now-1.month,
  badge: "Cool awesome Badge!",
  important: true,
  culture: true,
  blog: true,
  users: [ User.last, User.third ],
  tag_list: "скандалы, интриги, сегодня"
)
Bond.create!(post: post, bondable: widget)
Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

3.times do
  Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
end

6.times do
  Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
end

widget = Widget.create!(kind: :auto,
                        position: 3,
                        active: true,
                        landing: true)
3.times do
  post = Post.create!(
    image: open_file('img/3.jpg'),
    image_tizer: open_file('img/3.jpg'),
    image_social: open_file('img/3.jpg'),
    image_outside_link: image_link,
    image_tizer_outside_link: image_link,
    image_social_outside_link: image_link,
    creator_id: rand(1..3),
    tizer_type: :picture,
    title: Faker::Lorem.paragraph(1),
    tizer_title: Faker::Lorem.paragraph(1),
    tizer_text: Faker::Lorem.paragraph(1),
    status: :pending,
    created_at: Time.now-1.month,
    updated_at: Time.now-1.week,
    body: body,
    lead_status: true,
    lead_text: Faker::Lorem.paragraph(1),
    image_type: rand(0..1),
    template: rand(0..3),
    main_page: rand(0..1),
    media_type: rand(0..1),
    icon: rand(0..6),
    icon_type: rand(0..2),
    tizer_style: rand(0..1),
    caption: Faker::Lorem.paragraph(1),
    copyright: "Hello world!",
    publish: Time.now-1.month,
    badge: "Cool awesome Badge!",
    important: true,
    culture: true,
    blog: true,
    users: [ User.second, User.first ],
    tag_list: "завтрашний, интриги, расследования"
  )
  Bond.create!(post: post, bondable: widget)
  Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

  3.times do
    Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
  end

  6.times do
    Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
  end
end

widget = Widget.create!(kind: :wide,
                        position: 4,
                        active: true,
                        landing: true)
post = Post.create!(
  image: open_file('img/3.jpg'),
  image_tizer: open_file('img/3.jpg'),
  image_social: open_file('img/3.jpg'),
  image_outside_link: image_link,
  image_tizer_outside_link: image_link,
  image_social_outside_link: image_link,
  tizer_type: :picture,
  creator_id: rand(1..3),
  title: Faker::Lorem.paragraph(1),
  tizer_title: Faker::Lorem.paragraph(1),
  tizer_text: Faker::Lorem.paragraph(1),
  status: :pending,
  created_at: Time.now-1.month,
  updated_at: Time.now-1.week,
  body: body,
  lead_status: true,
  lead_text: Faker::Lorem.paragraph(1),
  image_type: rand(0..1),
  template: rand(0..3),
  main_page: rand(0..1),
  media_type: rand(0..1),
  icon: rand(0..6),
  icon_type: rand(0..2),
  tizer_style: rand(0..1),
  caption: Faker::Lorem.paragraph(1),
  copyright: "Hello world!",
  publish: Time.now-1.month,
  badge: "Cool awesome Badge!",
  important: true,
  culture: true,
  blog: true,
  users: [ User.second, User.first ],
  tag_list: "завтрашний, интриги, расследования"
)
Bond.create!(post: post, bondable: widget)
Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

3.times do
  Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
end

6.times do
  Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
end

widget = Widget.create!(kind: :auto,
                        position: 5,
                        active: true,
                        landing: true)
3.times do
  post = Post.create!(
    image: open_file('img/3.jpg'),
    image_tizer: open_file('img/3.jpg'),
    image_social: open_file('img/3.jpg'),
    image_outside_link: image_link,
    image_tizer_outside_link: image_link,
    image_social_outside_link: image_link,
    creator_id: rand(1..3),
    title: Faker::Lorem.paragraph(1),
    tizer_title: Faker::Lorem.paragraph(1),
    tizer_text: Faker::Lorem.paragraph(1),
    status: :pending,
    created_at: Time.now-1.month,
    updated_at: Time.now-1.week,
    body: body,
    tizer_type: :picture,
    lead_status: true,
    lead_text: Faker::Lorem.paragraph(1),
    image_type: rand(0..1),
    template: rand(0..3),
    main_page: rand(0..1),
    media_type: rand(0..1),
    icon: rand(0..6),
    icon_type: rand(0..2),
    tizer_style: rand(0..1),
    caption: Faker::Lorem.paragraph(1),
    copyright: "Hello world!",
    publish: Time.now-1.month,
    badge: "Cool awesome Badge!",
    important: true,
    culture: true,
    blog: true,
    users: [ User.second, User.first ],
    tag_list: "завтрашний, интриги, расследования"
  )
  Bond.create!(post: post, bondable: widget)
  Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

  3.times do
    Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
  end

  6.times do
    Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
  end
end


widget = Widget.create!(kind: :package,
                        position: 6,
                        active: true,
                        landing: true)
6.times do |index|
  post = Post.create!(
    image: open_file('img/3.jpg'),
    image_tizer: open_file('img/3.jpg'),
    image_social: open_file('img/3.jpg'),
    image_outside_link: image_link,
    image_tizer_outside_link: image_link,
    image_social_outside_link: image_link,
    creator_id: rand(1..3),
    title: Faker::Lorem.paragraph(1),
    tizer_title: Faker::Lorem.paragraph(1),
    tizer_text: Faker::Lorem.paragraph(1),
    status: :pending,
    created_at: Time.now-1.month,
    updated_at: Time.now-1.week,
    body: body,
    tizer_type: :picture,
    lead_status: true,
    lead_text: Faker::Lorem.paragraph(1),
    image_type: rand(0..1),
    template: rand(0..3),
    main_page: rand(0..1),
    media_type: rand(0..1),
    icon: rand(0..6),
    icon_type: rand(0..2),
    tizer_style: rand(0..1),
    caption: Faker::Lorem.paragraph(1),
    copyright: "Hello world!",
    publish: Time.now-1.month,
    badge: "Cool awesome Badge!",
    important: true,
    culture: true,
    blog: true,
    users: [ User.second, User.first ],
    tag_list: "завтрашний, интриги, расследования"
  )
  Bond.create!(post: post, bondable: widget)
  Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

  3.times do
    Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
  end

  6.times do
    Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
  end
end

widget = Widget.create!(kind: :banner,
                        position: 7,
                        landing: true)
post = Post.create!(
  image: open_file('img/1.jpg'),
  image_tizer: open_file('img/1.jpg'),
  image_social: open_file('img/1.jpg'),
  image_outside_link: image_link,
  image_tizer_outside_link: image_link,
  image_social_outside_link: image_link,
  creator_id: rand(1..3),
  tizer_type: :picture,
  title: Faker::Lorem.paragraph(1),
  tizer_title: Faker::Lorem.paragraph(1),
  tizer_text: Faker::Lorem.paragraph(1),
  status: :pending,
  created_at: Time.now-1.month,
  updated_at: Time.now-1.week,
  body: body,
  lead_status: true,
  lead_text: Faker::Lorem.paragraph(1),
  image_type: rand(0..1),
  template: rand(0..3),
  main_page: rand(0..1),
  media_type: rand(0..1),
  icon: rand(0..6),
  icon_type: rand(0..2),
  tizer_style: rand(0..1),
  caption: Faker::Lorem.paragraph(1),
  copyright: "Hello world!",
  publish: Time.now-1.month,
  badge: "Cool awesome Badge!",
  important: false,
  culture: true,
  blog: true,
  users: [ User.second, User.third ],
  tag_list: "скандалы, интриги, расследования"
)
Bond.create!(post: post, bondable: widget)
Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

3.times do
  Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
end

6.times do
  Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
end

widget = Widget.create!(kind: :blogs, position: 8, landing: true)
3.times do |index|
  post = Post.create!(
    image: open_file('img/2.jpg'),
    image_tizer: open_file('img/2.jpg'),
    image_social: open_file('img/2.jpg'),
    image_outside_link: image_link,
    image_tizer_outside_link: image_link,
    image_social_outside_link: image_link,
    creator_id: rand(1..3),
    title: Faker::Lorem.paragraph(1),
    tizer_title: Faker::Lorem.paragraph(1),
    tizer_text: Faker::Lorem.paragraph(1),
    status: :pending,
    tizer_type: :picture,
    created_at: Time.now-1.month,
    updated_at: Time.now-1.week,
    body: body,
    lead_status: true,
    lead_text: Faker::Lorem.paragraph(1),
    image_type: rand(0..1),
    template: rand(0..3),
    main_page: rand(0..1),
    media_type: rand(0..1),
    icon: rand(0..6),
    icon_type: rand(0..2),
    tizer_style: rand(0..1),
    caption: Faker::Lorem.paragraph(1),
    copyright: "Hello world!",
    publish: Time.now-1.month,
    badge: "Cool awesome Badge!",
    important: true,
    culture: true,
    blog: true,
    users: [ User.first, User.second ],
    tag_list: "скандалы, расследования, вчерашний"
  )
  Bond.create!(post: post, bondable: widget)
  Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])
end
puts "..... OK \n\n"

Widget.create!(kind: :auto, position: 9, landing: true)
Widget.create!(kind: :auto, position: 10, landing: true)
Widget.create!(kind: :package, position: 11, landing: true)

print 'Video Posts.......'
5.times do |index|
  post = Post.create!(
    image: open_file('img/3.jpg'),
    image_tizer: open_file('img/3.jpg'),
    image_social: open_file('img/3.jpg'),
    image_outside_link: 'https://youtu.be/l4-kItwN71c',
    image_tizer_outside_link: image_link,
    image_social_outside_link: image_link,
    creator_id: rand(1..3),
    title: Faker::Lorem.paragraph(1),
    tizer_title: Faker::Lorem.paragraph(1),
    tizer_text: Faker::Lorem.paragraph(1),
    status: :pending,
    created_at: Time.now-1.month,
    updated_at: Time.now-1.week,
    body: body,
    tizer_type: :youtube,
    lead_status: true,
    lead_text: Faker::Lorem.paragraph(1),
    image_type: rand(0..1),
    template: rand(0..3),
    main_page: rand(0..1),
    media_type: rand(0..1),
    icon: rand(0..6),
    icon_type: rand(0..2),
    tizer_style: rand(0..1),
    caption: Faker::Lorem.paragraph(1),
    copyright: "Hello world!",
    publish: Time.now-1.month,
    badge: "Cool awesome Badge!",
    important: false,
    culture: false,
    blog: true,
    users: [ User.last],
    tag_list: "скандалы, интриги"
  )
  Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

  3.times do
    Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
  end

  1.times do
    Bond.create!(post: post, bondable_type: "Genre", bondable_id: 7)
  end
end
puts "..... OK \n\n"


print 'Culture Widgets.......'

4.times do |index|
  Widget.create!(kind: :auto,
                 position: index+1,
                 culture: true,
                 active: true,
                 tag_list: "завтрашний, интриги, расследования")
end

4.times do |index|
  post = Post.create!(
    image: open_file('img/3.jpg'),
    image_tizer: open_file('img/3.jpg'),
    image_social: open_file('img/3.jpg'),
    image_outside_link: image_link,
    image_tizer_outside_link: image_link,
    image_social_outside_link: image_link,
    creator_id: rand(1..3),
    title: Faker::Lorem.paragraph(1),
    tizer_title: Faker::Lorem.paragraph(1),
    tizer_text: Faker::Lorem.paragraph(1),
    status: :progress,
    created_at: Time.now-1.month,
    updated_at: Time.now-1.week,
    body: body,
    tizer_type: :picture,
    lead_status: true,
    lead_text: Faker::Lorem.paragraph(1),
    image_type: rand(0..1),
    template: rand(0..3),
    main_page: rand(0..1),
    media_type: rand(0..1),
    icon: rand(0..6),
    icon_type: rand(0..2),
    tizer_style: rand(0..1),
    caption: Faker::Lorem.paragraph(1),
    copyright: "Hello world!",
    publish: Time.now-1.month,
    badge: "Cool awesome Badge!",
    important: false,
    culture: true,
    blog: true,
    users: [ User.second, User.third ],
    tag_list: "интриги, расследования"
  )
  Bond.create!(post: post, bondable: Widget.culture.first)
  Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

  3.times do
    Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
  end

  6.times do
    Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
  end
end


widget = Widget.create!(kind: 8, position: 2, culture: true, active: true)
3.times do |index|
  post = Post.create!(
    image: open_file('img/3.jpg'),
    image_tizer: open_file('img/3.jpg'),
    image_social: open_file('img/3.jpg'),
    image_outside_link: image_link,
    image_tizer_outside_link: image_link,
    image_social_outside_link: image_link,
    creator_id: rand(1..3),
    tizer_type: :picture,
    title: Faker::Lorem.paragraph(1),
    tizer_title: Faker::Lorem.paragraph(1),
    tizer_text: Faker::Lorem.paragraph(1),
    status: :pending,
    created_at: Time.now-1.month,
    updated_at: Time.now-1.week,
    body: body,
    lead_status: true,
    lead_text: Faker::Lorem.paragraph(1),
    image_type: rand(0..1),
    template: rand(0..3),
    main_page: rand(0..1),
    media_type: rand(0..1),
    icon: rand(0..6),
    icon_type: rand(0..2),
    tizer_style: rand(0..1),
    caption: Faker::Lorem.paragraph(1),
    copyright: "Hello world!",
    publish: Time.now-1.month,
    badge: "Cool awesome Badge!",
    important: false,
    culture: true,
    blog: true,
    users: [ User.second, User.third ],
    tag_list: "скандалы, интриги, расследования, сегодня"
  )
  Bond.create!(post: post, bondable: widget)
  Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

  3.times do
    Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
  end

  6.times do
    Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
  end
end

puts "..... OK \n\n"

puts "Topic Widgets.........."
3.times do |index|
  widget = Widget.create!(kind: :top, title: Faker::Lorem.paragraph(1))
  5.times do
    post = Post.create!(
      image: open_file('img/3.jpg'),
      image_tizer: open_file('img/3.jpg'),
      image_social: open_file('img/3.jpg'),
      image_outside_link: image_link,
      image_tizer_outside_link: image_link,
      image_social_outside_link: image_link,
      creator_id: rand(1..3),
      title: Faker::Lorem.paragraph(1),
      tizer_title: Faker::Lorem.paragraph(1),
      tizer_text: Faker::Lorem.paragraph(1),
      status: :pending,
      created_at: Time.now-1.month,
      updated_at: Time.now-1.week,
      body: body,
      tizer_type: :picture,
      lead_status: true,
      lead_text: Faker::Lorem.paragraph(1),
      image_type: rand(0..1),
      template: rand(0..3),
      main_page: rand(0..1),
      media_type: rand(0..1),
      icon: rand(0..6),
      icon_type: rand(0..2),
      tizer_style: rand(0..1),
      caption: Faker::Lorem.paragraph(1),
      copyright: "Hello world!",
      publish: Time.now-1.month,
      badge: "Cool awesome Badge!",
      important: false,
      culture: true,
      blog: true,
      users: [ User.second, User.third ],
      tag_list: "скандалы, интриги, расследования, сегодня, водка"
    )
    Bond.create!(post: post, bondable: widget)
    Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

    3.times do
      Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
    end

    6.times do
      Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
    end
  end
end
puts "..... OK \n\n"

puts "News Widget ....."
widget = Widget.create!(kind: :top,
                        title: Faker::Lorem.paragraph(1),
                        image: open_file('img/3.jpg'),
                        news: true,
                        position: 1,
                        active: true)
5.times do
  post = Post.create!(
    image: open_file('img/1.jpg'),
    image_tizer: open_file('img/2.jpg'),
    image_social: open_file('img/1.jpg'),
    image_outside_link: image_link,
    image_tizer_outside_link: image_link,
    image_social_outside_link: image_link,
    creator_id: rand(1..3),
    tizer_type: :picture,
    title: Faker::Lorem.paragraph(1),
    tizer_title: Faker::Lorem.paragraph(1),
    tizer_text: Faker::Lorem.paragraph(1),
    status: :pending,
    created_at: Time.now-1.month,
    updated_at: Time.now-1.week,
    body: body,
    lead_status: true,
    lead_text: Faker::Lorem.paragraph(1),
    image_type: rand(0..1),
    template: rand(0..3),
    main_page: rand(0..1),
    media_type: rand(0..1),
    icon: rand(0..6),
    icon_type: rand(0..2),
    tizer_style: rand(0..1),
    caption: Faker::Lorem.paragraph(1),
    copyright: "Hello world!",
    publish: Time.now-1.month,
    badge: "Cool awesome Badge!",
    important: false,
    users: [ User.second, User.third ],
    tag_list: "скандалы, интриги, расследования"
  )
  Bond.create!(post: post, bondable: widget)
  Bullet.create([{text: "Hello world!", post: post}, {text: "Hello world!", post: post}])

  3.times do
    Bond.create!(post: post, bondable_type: "Topic", bondable_id: rand(1..5))
  end

  6.times do
    Bond.create!(post: post, bondable_type: "Genre", bondable_id: rand(1..9))
  end
end
puts "..... OK \n\n"
